# Awesome NeoVim Plugins
<br/>
## This is where I will have a list of awesome plugins that work well with NeoVim at the most recent version on the [NeoVim Repository](https://github.com/neovim/neovim)
<br/>

* NeoBundle
* NeoMake
* NeoTerm
* Deoplete
* TernJS
* UltiSnips
* Supertab
* Airline
* CtrlP
* Surround
* NERDCommenter
* NERDTree
* EasyMotion
* EasyClip
* BufExplorer
* Fugitive
* Obsession
* Eunuch
* Repeat
* Ag.vim
* Editorconfig
* Matchit
* MatchTag
* DelimitMate
* IndentLine
* Mundo
* Xterm Color Table
* Multiple Cursors
* YaJS
* vim-javascript
* Have a suggestion? Open a pull request!
